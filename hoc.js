"use strict";
// Higher Order Functions
// 1. Accepts a function as an argument
// 2. Returns a new function
var withCount = function (fn) {
    var count = 0;
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.log("Call count: " + ++count);
        return fn.apply(void 0, args);
    };
};
var add = function (x, y) { return x + y; };
var countedAdd = withCount(add);
console.log(countedAdd(1, 2));
console.log(countedAdd(1, 2));
console.log(countedAdd(1, 2));
console.log(countedAdd(1, 2));
