// Higher Order Functions

// 1. Accepts a function as an argument
// 2. Returns a new function

const withCount = (fn: any) => {
  let count = 0;

  return (...args: any) => {
    console.log(`Call count: ${++count}`);
    return fn(...args);
  }
}

const add = (x: number, y: number): number => x + y;

const countedAdd = withCount(add);

console.log(countedAdd(1, 2));
console.log(countedAdd(1, 2));
console.log(countedAdd(1, 2));
console.log(countedAdd(1, 2));