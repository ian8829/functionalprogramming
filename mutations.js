"use strict";
// Immutable Data
// Mutable = can be changed after creation
// Immutable = can't be changed after creation
// Muttable class
var MutableGlass = /** @class */ (function () {
    function MutableGlass(content, amount) {
        this.content = content;
        this.amount = amount;
    }
    MutableGlass.prototype.takeDrink = function (value) {
        this.amount = Math.max(this.amount - value, 0);
        return this;
    };
    return MutableGlass;
}());
var mg1 = new MutableGlass('water', 100);
var mg2 = mg1.takeDrink(20);
// console.log(mg1 === mg2);
// console.log(mg1.amount === mg2.amount);
var ImmutableGlass = /** @class */ (function () {
    function ImmutableGlass(content, amount) {
        this.content = content;
        this.amount = amount;
    }
    ImmutableGlass.prototype.takeDrink = function (value) {
        return new ImmutableGlass(this.content, Math.max(this.amount - value, 0));
    };
    return ImmutableGlass;
}());
var ig1 = new ImmutableGlass('water', 100);
var ig2 = ig1.takeDrink(20);
console.log("ig1? " + JSON.stringify(ig1) + ", ig2? " + JSON.stringify(ig2));
console.log(ig1 === ig2);
console.log(ig1.amount === ig2.amount);
